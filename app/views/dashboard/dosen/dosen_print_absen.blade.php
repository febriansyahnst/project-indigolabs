<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">
    <link rel="shortcut icon" href="/images/favicon.png">

    <title>Blank page</title>
    <!--Core CSS -->
   

    {{ HTML::style('bs3/css/bootstrap.min.css') }}
    {{ HTML::style('css/bootstrap-reset.css') }}
    {{ HTML::style('font-awesome/css/font-awesome.css') }}
    <!--<link href="bs3/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-reset.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet" />-->

    <!-- Custom styles for this template -->
    {{ HTML::style('/css/style.css') }}
    {{ HTML::style('/css/style-responsive.css') }}
    <!--<link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet" />-->

    {{ HTML::style('/js/data-tables/DT_bootstrap.css') }}
    <!--<link rel="stylesheet" href="js/data-tables/DT_bootstrap.css" />-->
    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]>
    <script src="js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body>

	
		
            <div class="row">
                <div class="col-md-12">
                    <h3 class="text-center">Nama Mata Kuliah | Kelas </h3>
                </div>
            </div>
			<div class="row">
                <div class="col-sm-12">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-condensed">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>NIM</th>
                                    <th>Nama Mahasiswa</th>
                                    <th>Kelas</th>
                                    <th>Shift</th>
                                    <th>Modul I</th>
                                    <th>Modul II</th>
                                    <th>Modul III</th>
                                    <th>Modul IV</th>
                                    <th>Modul V</th>
                                    <th>Modul VI</th>
                                    <th>Kehadiran</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>1106110009</td>
                                    <td>YUSUF RAHMADI</td>
                                    <td>SI3601</td>
                                    <td>Senin,<br> 06.30-08.10</td>
                                    <td>Hadir</td>
                                    <td>Hadir</td>
                                    <td>Hadir</td>
                                    <td>Hadir</td>
                                    <td>Hadir</td>
                                    <td>Alpha</td>
                                    <td>5/6</td>
                                 </tr>
                                 <tr>
                                    <td>1</td>
                                    <td>1106110009</td>
                                    <td>YUSUF RAHMADI</td>
                                    <td>SI3601</td>
                                    <td>Senin,<br> 06.30-08.10</td>
                                    <td>Hadir</td>
                                    <td>Hadir</td>
                                    <td>Hadir</td>
                                    <td>Hadir</td>
                                    <td>Hadir</td>
                                    <td>Alpha</td>
                                    <td>5/6</td>
                                 </tr>
                                 <tr>
                                    <td>1</td>
                                    <td>1106110009</td>
                                    <td>YUSUF RAHMADI</td>
                                    <td>SI3601</td>
                                    <td>Senin,<br> 06.30-08.10</td>
                                    <td>Hadir</td>
                                    <td>Hadir</td>
                                    <td>Hadir</td>
                                    <td>Hadir</td>
                                    <td>Hadir</td>
                                    <td>Alpha</td>
                                    <td>5/6</td>
                                 </tr>
                                                                       
                            </tbody>

                        </table>
                    </div>
                </div>


            </div>

            <div class="row">
                <div class="col-md-12">
                    <h6 class="text-right"> Dicetak oleh Nama Lengkap, pada tanggal 00-00-00 00:00:00 </h6>
                </div>
            </div>



</body>
</html>