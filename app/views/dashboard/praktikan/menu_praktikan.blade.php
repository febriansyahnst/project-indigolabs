<aside>
    <div id="sidebar" class="nav-collapse">
        <!-- sidebar menu start-->            <div class="leftside-navigation">
            <ul class="sidebar-menu" id="nav-accordion">
            <li>
                <a href="/">
                    <i class="fa fa-dashboard"></i>
                    <span>Dashboard</span>
                </a>
            </li>
			<li>
                <a href="/profile">
                    <i class="fa fa-user"></i>
                    <span>Profil</span>
                </a>
            </li>
            <li class="sub-menu">
                <a href="javascript:;">
                    <i class="fa fa-laptop"></i>
                    <span>Registrasi</span>
                </a>
                <ul class="sub">
                    <li><a href="/praktikan/RegisterPraktikum">Registrasi Praktikum</a></li>
                </ul>
            </li>
            
        </ul></div>        
<!-- sidebar menu end-->
    </div>
</aside>